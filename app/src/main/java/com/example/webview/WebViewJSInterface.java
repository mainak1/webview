package com.example.webview;

import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class WebViewJSInterface {
    private MainActivity context;

    public WebViewJSInterface(MainActivity context) {
        this.context = context;
    }

    @JavascriptInterface
    public String getToast() {
        return context.getLocalClassName();
    }

    @JavascriptInterface
    public void makeToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
