export const LIGHTER1 = `
<div class="savd-highlighter">
        <div class="savd-lighter">
            <div class="button-closed" onclick="switchToHighlightMode();">
                <div style="background: #f48fb1;"></div>
                <div style="background: #fff59d; margin-left: 8px;"></div>
                <div style="background: #90caf9; margin-left: 16px;"></div>
                <div style="background: #ffcc80; margin-left: 24px;"></div>
            </div>

            <span class="material-icons">note_add</span>
            <span class="material-icons">file_copy</span>
            <span class="material-icons">share</span>
            <span class="material-icons">more_vert</span>
        </div>
</div>
`

export const LIGHTER2 = `
<div class="savd-highlighter">
        <div class="savd-lighter">
            <button style="background: #f48fb1;" onclick="highlight('#f48fb1');"></button>
            <button style="background: #fff59d;" onclick="highlight('#fff59d');"></button>
            <button style="background: #90caf9;" onclick="highlight('#90caf9');"></button>
            <button style="background: #ffcc80;" onclick="highlight('#ffcc80');"></button>
        </div>
</div>
`