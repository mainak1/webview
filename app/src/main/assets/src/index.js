import Highlighter from "highlight";
import './styles.css';
import { LIGHTER1, LIGHTER2 } from './html';

document.head.append(`<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">`);

function getLighter() {
    let el = document.getElementById("savd-lighter");
    if (!el) {
        el = document.createElement("div");
        el.id = "savd-lighter";
        el.style.position = 'absolute';
        el.style.zIndex = '2147483650';
        document.body.appendChild(el);
    }

    const { top, left } = Highlighter.getMenuPosition(64, 264);
    el.style.top = `${top}px`;
    el.style.left = `${left}px`;

    return el;
}

function removeLighter() {
    let el = document.getElementById("savd-lighter");
    if (el) {
        el.parentNode.removeChild(el);
    }
}

function getMenuOnSelectionChange() {
    const range = getSelection()?.getRangeAt(0);
    if (range.toString() !== '') {
        const lighter = getLighter();
        lighter.innerHTML = LIGHTER1;
        console.log("open");
    } else {
        removeLighter();
        console.log("hide");
    }
}

window.highlight = (color) => {
    const data = Highlighter.createHighlight(color);
    Highlighter.restoreHighlight({
        id: 'dsnfjkdsnf',
        color,
        data: data.data,
    });
    removeLighter();
}

window.switchToHighlightMode = (e) => {
    e.preventDefault();
    let el = document.getElementById("savd-lighter");
    if (el) {
        el.innerHTML = LIGHTER2;
    }
}

console.log("started")

document.onselectionchange = () => getMenuOnSelectionChange();