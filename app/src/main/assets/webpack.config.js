const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

var config = {
    devtool: 'inline-source-map',
    entry: path.join(__dirname, 'src', 'index.js'),
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: path.join(__dirname, '/node_modules'),
                loader: 'babel-loader',
            },
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(jpe?g|svg|gif|png|ico)$/i,
                loader: 'url-loader',
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.html', '.css', 'jsx'],
    },
    plugins: [
        new CleanWebpackPlugin()
    ],
}

module.exports = (env, argv) => {

    if (argv.mode === 'development') {
        config.mode = 'development';
        config.devtool = 'inline-source-map';
        config.devServer = {
            contentBase: './dist',
        };
    }

    if (argv.mode === 'production') {
        config.mode = 'production';
    }

    return config;
};